<?xml version="1.0" encoding="US-ASCII"?>
<!-- This template is for creating an Internet Draft using xml2rfc,
    which is available here: http://xml.resource.org. -->
<!DOCTYPE rfc SYSTEM "rfc2629.dtd">
<?xml-stylesheet type='text/xsl' href='rfc2629.xslt' ?>
<!-- used by XSLT processors -->
<!-- For a complete list and description of processing instructions (PIs), 
    please see http://xml.resource.org/authoring/README.html. -->
<!-- Below are generally applicable Processing Instructions (PIs) that most I-Ds might want to use.
    (Here they are set differently than their defaults in xml2rfc v1.32) -->
<?rfc strict="yes" ?>
<!-- give errors regarding ID-nits and DTD validation -->
<!-- control the table of contents (ToC) -->
<?rfc toc="yes"?>
<!-- generate a ToC -->
<?rfc tocdepth="4"?>
<!-- the number of levels of subsections in ToC. default: 3 -->
<!-- control references -->
<?rfc symrefs="yes"?>
<!-- use symbolic references tags, i.e, [RFC2119] instead of [1] -->
<?rfc sortrefs="yes" ?>
<!-- sort the reference entries alphabetically -->
<!-- control vertical white space 
    (using these PIs as follows is recommended by the RFC Editor) -->
<?rfc compact="yes" ?>
<!-- do not start each main section on a new page -->
<?rfc subcompact="no" ?>
<!-- keep one blank line between list items -->
<!-- end of list of popular I-D processing instructions -->
<rfc category="exp" docName="draft-briscoe-tsvwg-aqm-sce-l4s-00"
     ipr="trust200902" updates="">
  <!-- category values: std, bcp, info, exp, and historic
    ipr values: trust200902, noModificationTrust200902, noDerivativesTrust200902,
       or pre5378Trust200902
    you can add the attributes updates="NNNN" and obsoletes="NNNN" 
    they will automatically be output with "(if approved)" -->

  <!-- ***** FRONT MATTER ***** -->

  <front>
    <!-- The abbreviated title is used in the page header - it is only necessary if the 
       full title is longer than 39 characters -->

    <title abbrev="DualQ Coupled AQMs">Per-Flow-Queue AQM Support for both SCE
    and L4S</title>

    <author fullname="Greg White" initials="G." surname="White">
      <organization>CableLabs</organization>

      <address>
        <postal>
          <street/>

          <city>Louisville, CO</city>

          <country>US</country>
        </postal>

        <email>g.white@cablelabs.com</email>
      </address>
    </author>

    <author fullname="Bob Briscoe" initials="B." role="editor"
            surname="Briscoe">
      <organization>CableLabs</organization>

      <address>
        <postal>
          <street/>

          <country>UK</country>
        </postal>

        <email>ietf@bobbriscoe.net</email>

        <uri>http://bobbriscoe.net/</uri>
      </address>
    </author>

    <date day="25" month="March" year="2019"/>

    <area>Transport</area>

    <workgroup>Transport Area working group (tsvwg)</workgroup>

    <keyword>Internet-Draft</keyword>

    <keyword>I-D</keyword>

    <abstract>
      <t>This memo proposes an enhancement to an node that supports Some
      Congestion Experienced (SCE) marking to allow SCE experiments to better
      coexist with L4S experiments on the Internet.</t>
    </abstract>
  </front>

  <middle>
    <section anchor="dualq_intro" title="Introduction">
      <t>This brief document assumes that readers understand ECN <xref
      target="RFC3168"/>, as updated by <xref target="RFC8311"/>, and that
      they know or will find out about both the SCE and L4S approaches from
      the references. Briefly the two approaches are:<list style="symbols">
          <t>Low Latency Low Loss Scalable throughput (L4S) <xref
          target="I-D.ietf-tsvwg-l4s-arch"/> achieves ultra-low queuing delay
          (consistently below 1-2ms) even when L4S flows are capacity-seeking
          (like TCP) and even when they all share a First in First Out (FIFO)
          queue. The low latency of L4S flows can be isolated from classic
          traffic, either using the DualQ Coupled AQM <xref
          target="I-D.ietf-tsvwg-aqm-dualq-coupled"/>) or per-flow
          queuing.</t>

          <t>The SCE approach <xref target="I-D.morton-taht-sce"/> also aims
          to ensure very low queuing delay when flows are capacity seeking. It
          separates flows capable of ultra-low queuing delay from classic
          flows using per-flow queuing.</t>
        </list></t>

      <t>This document proposes a way in which both approaches can better
      co-exist in the Internet. It is not intended to compare the two
      approaches - that's for elsewhere. The focus is solely on
      inter-operation with each other.</t>
    </section>

    <section title="Coexistence Proposal">
      <t>Currently, 'L4S behaviour' means an AQM that alters more ECT(1)
      packets to CE the more the instantaneous queue exceeds a shallow
      threshold.</t>

      <t>'SCE behaviour' means an AQM that alters more ECT(0) packets to
      ECT(1) the more the instantaneous queue exceeds a shallow threshold. And
      it alters more ECT(0) or ECT(1) to CE the more the queue persistently
      exceeds a deeper target, e.g. as driven by CoDel <xref
      target="RFC8289"/> or PIE <xref target="RFC8033"/>.</t>

      <t>Currently, if per-flow queuing (FQ) is implemented, an L4S or an SCE
      approach can be used. The proposal is for an FQ AQM that embodies both
      approaches. It is easier to describe as a modification to an SCE AQM, as
      follows.</t>

      <t>To allow both schemes to co-exist, a per-flow queue that already
      supported SCE would need the following minor alteration:<list
          style="empty">
          <t>An SCE AQM's flow state defaults to SCE behaviour, until it sees
          an ECT(1) packet when it switches its flow state to L4S behaviour,
          unless it has ever seen or ever sees an ECT(0) packet.</t>
        </list></t>

      <t>To be clear, flow state in an SCE network node is expected to be
      ephemeral, and is not expected to survive as long as the e2e flow state
      (but it's fine if it does). In particular, the above logic can be reset
      when a queue is moved to the "Empty" state <xref target="RFC8290"/>.</t>

      <t>The reason this proposal improves compatibility is that a
      flow-queuing node can recognize the type of the flow and adapt its
      behaviour accordingly. And even though an SCE node introduces some
      ECT(1) into a flow, if this hits another SCE node downstream, it can
      tell that the ECT(1) did not come from a Prague sender, because there is
      some ECT(0) mixed in the same flow.</t>

      <t>In L4S specification terms, this would require a tweak to the section
      in the existing L4S requirement about network nodes with transport-layer
      awareness (Section 5.3 of <xref target="I-D.ietf-tsvwg-ecn-l4s-id"/>).
      In SCE network node implementation terms, this proposal would require an
      extra mode for an SCE network node and the different ECN-marking
      transition for any AQM within a flow-queue in this mode.</t>
    </section>

    <section title="Coexistence Cases">
      <t><xref target="Fig_scel4s_without"/> shows all the combinations of
      Sender Congestion Control (CC), and AQM without the proposed compromise.
      <xref target="Fig_scel4s_with"/> shows the same combinations with the
      proposed approach (using the abbreviation FQ3).</t>

      <t>In each figure, two AQMs are shown on the path, one upstream of the
      other, in order to show how two AQMs using all the combinations of the
      two approaches would interact. A 'Low Delay' outcome means as good as
      the state-of the art with FQ-CoDel <xref target="RFC8290"/> or PIE <xref
      target="RFC8033"/>, <xref target="RFC8034"/> (before these new schemes).
      'Ultra-Low-Delay' is the outcome hoped for from SCE and achieved by
      L4S.</t>

      <figure anchor="Fig_scel4s_without"
              title="Without the Proposed Compromise">
        <artwork><![CDATA[Sender CC    Upstream  Downstrm  Delay
             Box       Box

SCE-CC ----> FQ2 --->  FQ2       Ultra-Low
        \         \->  2QC       Low/Ultra-Low & Reordering
         \-> 2QC --->  FQ2       Low
                  \->  2QC       Low
Prague ----> FQ2 --->  FQ2       Low
        \         \->  2QC       Low
         \-> 2QC --->  FQ2       Low
                  \->  2QC       Ultra-Low

]]></artwork>

        <postamble>2QC = DualQ-Coupled AQM; FQ2 = FQ-2-Threshold (SCE)
        AQM</postamble>
      </figure>

      <t/>

      <figure anchor="Fig_scel4s_with" title="With the Proposed Compromise">
        <artwork><![CDATA[Sender CC    Upstream  Downstrm  Delay
             Box       Box

SCE-CC ----> FQ3 --->  FQ3       Ultra-Low
        \         \->  2QC       Low/Ultra-Low & Reordering
         \-> 2QC --->  FQ3       Low
                  \->  2QC       Low
Prague ----> FQ3 --->  FQ3       Ultra-Low
        \         \->  2QC       Ultra-Low
         \-> 2QC --->  FQ3       Ultra-Low
                  \->  2QC       Ultra-Low

]]></artwork>

        <postamble>2QC = DualQ-Coupled AQM; FQ3 = FQ-3-Mode AQM (the proposed
        compromise)</postamble>
      </figure>

      <t>Without the proposed compromise, it can be seen that the only
      combinations that give or potentially give ultra-low delay are those
      where all three elements on the path are from the same approach. In all
      the other case except one, the combination is no worse than the
      state-of-the-art today.</t>

      <t>The exception is where the three output states of the SCE protocol
      depart from the ground rules for ECN experimentation in <xref
      target="RFC8311"/>, which were assumed in the design of the L4S DualQ
      Coupled AQM. This particular incompatibility is not addressed or solved
      by the present proposal.</t>

      <t>With the proposed compromise, there are three more combinations where
      Ultra-Low Queuing Delay is achieved. It is not problematic that some
      cases do not achieve ultra-low delay - the SCE approach accepts that it
      will often not improve delay. Nonetheless, achieving ultra-low delay in
      more than half the cases is beneficial.</t>
    </section>

    <section anchor="dualq_Security_Considerations"
             title="Security Considerations">
      <t>This proposal allows a single ECT(0) packet to switch the (probably
      ephemeral) flow-state of an AQM into a different ECN-marking mode. It is
      not believed that this could be used as an attack vector, but
      intuitively it feels rather brittle.</t>
    </section>

    <section title="Acknowledgements">
      <t>Thanks to Jason Livingood for suggesting we try to work out a
      compromise proposal that makes both schemes work.</t>
    </section>
  </middle>

  <!--  *****BACK MATTER ***** -->

  <back>
    <references title="Normative References">
      <?rfc include='reference.RFC.3168'?>

      <?rfc include='reference.RFC.8311'?>
    </references>

    <references title="Informative References">
      <?rfc include='reference.RFC.8289'?>

      <?rfc include='reference.RFC.8290'?>

      <?rfc include='reference.RFC.8033'?>

      <?rfc include='reference.RFC.8034'?>

      <?rfc include='reference.I-D.morton-taht-sce'?>

      <?rfc include='reference.I-D.ietf-tsvwg-ecn-l4s-id'?>

      <?rfc include='reference.I-D.ietf-tsvwg-l4s-arch'?>

      <?rfc include='reference.I-D.ietf-tsvwg-aqm-dualq-coupled'?>
    </references>

    <!--    <section title="Change Log (to be Deleted before Publication)">
      <t>A detailed version history can be accessed at
      &lt;http://datatracker.ietf.org/doc/draft-briscoe-aqm-ecn-roadmap/history/&gt;</t>

      <t><list style="hanging">
          <t hangText="From briscoe-...-00 to briscoe-...-01:">Technical
          changes:<list style="symbols">
              <t/>
            </list>Editorial changes:<list style="symbols">
              <t/>
            </list></t>
        </list></t>
    </section>
-->
  </back>
</rfc>
